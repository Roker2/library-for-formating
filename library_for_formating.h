//Библиотека для форматирования
//Версия 1.5.0
//Требования:
//1. C++ 11

#ifndef LIBRARY_FOR_FORMATING_H_INCLUDED
#define LIBRARY_FOR_FORMATING_H_INCLUDED

#include <iostream>
#include <string>
#include <conio.h>

using namespace std;
//Ввод количества столбов массива
void vvodstolb (int *N)
{
    cout<<"Введите количество столбцов массива:";
    cin>>*N;
    if (*N<1)
    {
        cout<<"Ошибка! Количество столбцов массива не может быть нулевым или отрицательным!\n";
        vvodstolb (N);
    }
}
//Ввод количества строк массива
void vvodstroka (int *M)
{
    cout<<"Введите количество строк массива:";
    cin>>*M;
    if (*M<1)
    {
        cout<<"Ошибка! Количество строк массива не может быть нулевым или отрицательным!\n";
        vvodstroka (M);
    }
}
//Проверка на четность
bool chet (int a)
{
    if(a%2==0)
        return 0;//четное
    else
        return 1;//нечетное
}

void vvodkolichestvo (int *N)
{
    cout<<"Введите количество элементов массива:";
    cin>>*N;
    if (*N<1)
    {
        cout<<"Ошибка! Количество элементов массива не может быть нулевым или отрицательным!\n";
        vvodkolichestvo (N);
    }
}
//Замена с экономией памяти
void zamena (int *a, int *b)
{
    *a = *a+*b;
    *b = *a-*b;
    *a = *a-*b;
}

void zamena (float *a, float *b)
{
    *a = *a+*b;
    *b = *a-*b;
    *a = *a-*b;
}

void zamena(char *a, char *b)
{
    *a = *a+*b;
    *b = *a-*b;
    *a = *a-*b;
}
//Замена строк без экономии памяти
void zamena(string *a, string *b)
{
    string c;
    c = *a;
    *a = *b;
    *b = c;
}
//Установка русского языка, использовать, если есть краказябы
void setRUS ()
{
    setlocale(LC_ALL, "Russian");
}
//Сумма элементов массива для массивов типа float
float mass_summ (float *mass, int size_mass)
{
    float summa=0;
    for(int i=0; i<size_mass; i++)
        summa+=mass[i];
    return summa;
}
//Сумма элементов массива для массивов типа int
int mass_summ (int *mass, int size_mass)
{
    int summa=0;
    for(int i=0; i<size_mass; i++)
        summa+=mass[i];
    return summa;
}
//Возведение числа в степень
int stepen (int a, int c)
{
    int b=1;
    for(int i=1; i<=c; i++)
        b*=a;
    return b;
}

float stepen(float a, int c)
{
    float b=1;
    for(int i=1; i<=c; i++)
        b*=a;
    return b;
}
//Перевод целых положительных чисел из 10-чной системы счисления в другую (меньше 10)
//b - число, с - число основания
int tentoan (int b, int c)
{
    int bin=0;
    for(int i=0; b>0; i++)
    {
        bin+=(b%c)*stepen(10, i);
        b/=c;
    }
    return bin;
}
//Перевернуть строку по горизонтали
void razvorot_str(string *str)
{
    for(int i=0; i<(*str).length()/2; i++)
    {
        zamena(&(*str)[i], &(*str)[(*str).length()-i-1]);
    }
}
/*
Добавляет символ в нужный вам элемент строки, сдвигая оставшуюся
Добавляет символ в следующий элемент после того, что Вы указали, то есть,
чтобы добавить символ в начало, надо вбить -1
По умолчанию стоит пробел
*/
void ch_in_str (string *str, int i, char ch=' ')
{
    //string mystr = *str;
    int sizestr = (*str).length();
    (*str)+=(*str)[sizestr-1];
    sizestr++;
    (*str)[sizestr-1]=ch;
    for(int viol=sizestr-1; viol-1>i; viol--)
    {
        zamena(&(*str)[viol], &(*str)[viol-1]);
    }
}
//Данная функция удаляет нужный элемент строки
void del_ch (string *str, int i)
{
    //string mystr=*str;
    (*str)[i]=' ';
    for(int viol=i; viol<(*str).length()-1; viol++)
    {
        //cout<<"Заменяю элементы "<<viol+1<<" и "<<viol+2<<endl;
        zamena(&(*str)[viol], &(*str)[viol+1]);
    }
    (*str).pop_back();
    //*str=mystr;
}
//Длина числа
int intLength(int i) {
    int l=0;
    for(;i;i/=10)
        l++;
    return l==0 ? 1 : l;
}
//Вычлиняет нужную вам цифру из целого числа
int element_chisla (int chislo, int i)
{
        int tmp1=chislo/stepen(10, i+1);
        return (chislo-chislo%stepen(10, i)-tmp1*stepen(10, i+1))/stepen(10, i);
}
//Вычисление факториала
int factorial (int n)
{
    int temp=1;
    for(int i=2; i<=n; i++)
    {
        temp*=i;
    }
    return temp;
}

//Вычисляет модуль числа
int module_number (int a)
{
    if(a < 0)
        return (a * (-1));
    else
        return a;
}

float module_number (float a)
{
    if(a < 0)
        return (a * (-1));
    else
        return a;
}
//Ожидание ввода любой кнопки для продолжения
void waiting_input ()
{
    cout<<"Press any key...";
    getch();
    cout<<endl;
}
//Выводит сообщение об ошибке, после ожидает ввод символа
void error_message ()
{
    cout<<"Error! ";
    waiting_input();
}
//Очищяет терминал
void clear_terminal()
{
    system("cls");
}
#endif // LIBRARY_FOR_FORMATING_H_INCLUDED
